﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	Image bkImg;
	Image jsImg;
	public Vector3 inputVector;
	public Color drgColour;
	void Start()
	{
		bkImg = GetComponent<Image> ();
		jsImg = transform.GetChild (0).GetComponent<Image>();
	}



	public virtual void OnPointerDown(PointerEventData ped)
	{
		OnDrag(ped);
	}

	public virtual void OnPointerUp(PointerEventData ped)
	{
		inputVector = Vector3.zero;
		jsImg.rectTransform.anchoredPosition = Vector3.zero;
		bkImg.color = Color.white;
	}

	public virtual void OnDrag(PointerEventData ped)
	{
		Vector2 pos;
		bkImg.color = drgColour;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (bkImg.rectTransform, ped.position, ped.pressEventCamera, out pos)) 
		{
			pos.x = (pos.x / bkImg.rectTransform.sizeDelta.x);
			pos.y = (pos.y / bkImg.rectTransform.sizeDelta.y);

			inputVector = new Vector3(pos.x*2, 0, pos.y*2);	
			//Debug.Log(inputVector);
			inputVector = (inputVector.magnitude > 1.0f)?inputVector.normalized:inputVector;

			jsImg.rectTransform.anchoredPosition = new Vector3(inputVector.x * (bkImg.rectTransform.sizeDelta.x/3), inputVector.z * (bkImg.rectTransform.sizeDelta.y/3));

		}
	}


}
