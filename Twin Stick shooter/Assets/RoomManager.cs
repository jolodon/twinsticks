﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class RoomManager : MonoBehaviour 
{
	public GameObject player;
	GameObject spawnPoint;
	int roomID;

	public static int counter = 1;
	public Text roomText;
	public GameObject[] spawnPoints = new GameObject[6];
	public GameObject[] roomsPrefab = new GameObject[6];
	//GameObject rooms;

	// Use this for initialization
	void Start () 
	{
		//blackFade.rectTransform.localScale = new Vector2 (Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void LoadRoom()
	{
		RoomManager.counter++;
		roomID = Random.Range (0, 6);
		spawnPoint = spawnPoints [roomID];
		roomsPrefab [roomID].SetActive (true);
		player.transform.position = spawnPoint.transform.position;
		player.transform.rotation = spawnPoint.transform.localRotation;
		player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		roomText.text = "Room " + counter;

		for (int i = 0; i < roomsPrefab.Length; i++) 
		{
			if (i !=  roomID) 
			{
				roomsPrefab [i].SetActive (false);

			}
		}


	}



}
