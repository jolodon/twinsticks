﻿using UnityEngine;
using System.Collections;


public class PlayerJoyMovement : MonoBehaviour {


	float xAxis;
	float zAxis;

	public GameObject joy;
	public int speedDivider; 
	public int rotationDamp;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		xAxis = joy.GetComponent<Joystick> ().inputVector.x;
		zAxis = joy.GetComponent<Joystick> ().inputVector.z;

		Vector3 e = new Vector3 (0 , 0 , (zAxis/ speedDivider) );

		Vector3 r = new Vector3 (0, (xAxis / rotationDamp) , 0);


		if (xAxis >= -0.5 && xAxis <= 0.5) 
		{
			transform.Translate (e);
		}

		if (zAxis >= -0.8 && zAxis <= 0.8) 
		{
			transform.Rotate (r);
		}
	
	}
}
