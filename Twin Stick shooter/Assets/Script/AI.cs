﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour 
{
	NavMeshAgent nav;
	public Transform player;

	public float acceleration = 2f;
	public float deceleration = 60f;
	public float closeEnoughMeters = 4f;

	public float distance;

	// Use this for initialization
	void Awake ()
	{
		nav = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		//transform.LookAt (player.transform);
		distance = Vector3.Distance(this.transform.position, player.transform.position);
		nav.SetDestination (player.position);
		nav.acceleration = (nav.remainingDistance < closeEnoughMeters) ? deceleration : acceleration;


		Vector3 targetPostition = new Vector3( player.position.x, 
			this.transform.position.y, 
			player.position.z ) ;
		this.transform.LookAt( targetPostition ) ;
//		if (distance <= 4)
//		{
//			nav.enabled = false;
//		} 

		/*if (timer >= countdown)
		{
			stunned = false;
			timer = 0;
			nav.Resume ();

		} */
	}

	void OnTriggerEnter(Collider other)
	{
		//for colliding with odder ting men
	}
		
}

