﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Naming : MonoBehaviour 
{
	public Text nameBox;

	string name;

	Transform head;
	Transform torso;
	Transform leg;

	// Use this for initialization
	void Start () 

	{
		head = this.transform.GetChild (0);
		torso = this.transform.GetChild (1);
		leg = this.transform.GetChild (2);

	}
	
	// Update is called once per frame
	void Update () 
	{
		
		name = leg.GetComponent<Randomiser> ().partName + " " + torso.GetComponent<Randomiser> ().partName + head.GetComponent<Randomiser> ().partName;
		nameBox.text = name;
	}
}
