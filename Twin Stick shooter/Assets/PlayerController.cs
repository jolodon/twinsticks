﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{

	public Image FadeImg;

	public float fadeSpeed;
	public GameObject manager;

	RoomManager rm;
	GameObject spawnPoint;

	// Use this for initialization




	void Start () 
	{
		spawnPoint = GameObject.Find ("Spawn Point");	
		transform.position = spawnPoint.transform.position;
		transform.rotation = spawnPoint.transform.localRotation;
	}
	
	// Update is called once per frame
	void OnCollisionEnter (Collision other) 
	{
		
		if (other.gameObject.tag == "Door") 
		{
			StartCoroutine ("Fade");
		}
	}

	void FadeToClear()
	{
		// Lerp the colour of the image between itself and transparent.
		FadeImg.color = Color.Lerp(FadeImg.color, Color.clear, fadeSpeed * Time.deltaTime / 50);
	}


	void FadeToBlack()
	{
		// Lerp the colour of the image between itself and black.
		FadeImg.color = Color.Lerp(FadeImg.color, Color.white, fadeSpeed * Time.deltaTime /50 );
	}

	public IEnumerator Fade()
	{
		manager.GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (0.75f);
		manager.GetComponent<RoomManager> ().LoadRoom ();
		yield return new WaitForSeconds (0.5f);
		manager.GetComponent<Fading> ().BeginFade (-1);
		yield return null;


	}
}
